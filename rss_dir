#!/usr/bin/env python

from feedgen.feed import FeedGenerator
import feedparser
import os
import re
import time
import datetime
import pytz
import eyed3
from cmdlineapi import CmdlineApi


class RssDir(CmdlineApi):

    def __init__(self):
        CmdlineApi.__init__(self)
        self.cmd_base_desc = "Manage rss from files in directory"

    def _cmdline_generate_args(self, parser):
        parser.description = "generate rss file"
        parser.add_argument('--baseurl', action='store',
                            dest='baseurl',
                            help='Base url for rss files',
                            required=1)
        parser.add_argument('--logo_location', action='store',
                            dest='logo_location',
                            help='Location of thumbnail',
                            required=1)
        parser.add_argument('--title', action='store',
                            dest='title',
                            help='Title of rss feed',
                            required=1)
        parser.add_argument('--subtitle', action='store',
                            dest='subtitle',
                            help='subtitle',
                            default='subtitle')
        parser.add_argument('--author', action='store',
                            dest='author',
                            help='author name',
                            required=1)
        parser.add_argument('--file_name', action='store',
                            dest='rss_file_name',
                            help='rss file name (optional)',
                            default='rss.xml',)
        parser.add_argument('--split_delim', action='store',
                            dest='split_delim',
                            help='file name split delimiter for getting id',
                            default='')
        parser.add_argument('--split_pos', action='store',
                            dest='split_pos',
                            help='position of string to grab for id',
                            type=int,
                            default=0)

        parser.add_argument('--update_existing', action='store_true',
                            dest='update_existing',
                            help='Update existing entries in feed instead\
                                  of leaving them alone')

        return parser

    # sorting accuracy is not imperative here
    @staticmethod
    def try_split_pos(my_string, redelim, index):
        try:
            val = re.split(r''+redelim, my_string)[index]
        except (IndexError):
            return my_string
        try:
            return int(val)
        except (ValueError, TypeError):
            return val

    def _cmdline_generate(self, args):
        baseurl = args.baseurl + "/"
        logo_location = args.logo_location
        title = args.title
        subtitle = args.subtitle
        author = {'name': args.author, 'email': 'john@example.de'}
        rss_file_name = args.rss_file_name
        split_delim = args.split_delim
        split_pos = args.split_pos
        update_existing = args.update_existing

        fg = FeedGenerator()
        fg.id(baseurl)
        fg.title(title)
        fg.author(author)
        fg.logo(logo_location)
        fg.subtitle(subtitle)
        fg.link(href=baseurl+rss_file_name, rel='self')
        fg.language('en')

        # load existing rss
        myfeed = {}
        if os.path.exists(rss_file_name):
            myfeed = feedparser.parse(rss_file_name)
            # map by id
            myfeed = dict((f.id, f) for f in myfeed.entries)

        filenames = os.listdir('./')

        filenames.sort(key=lambda x: self.try_split_pos(x, split_delim, split_pos))

        i = 0
        for filename in filenames:
            if not re.search(r'\.(mp3|mpeg|flac|wav)$', filename):
                continue

            print filename

            podid = i

            if split_delim:
                podid = self.try_split_pos(filename, split_delim, split_pos)

            fe = fg.add_entry()

            existing_entry = myfeed.get(podid)

            date_obj = {}
            file_title = ''

            if existing_entry and not update_existing:
                timestamp = time.mktime(existing_entry.published_parsed)
                date_obj = datetime.datetime.fromtimestamp(timestamp,
                                                           tz=pytz.utc)
                file_title = existing_entry.title
            else:
                # add time between publish dates for sorting in some apps
                # TODO: smarter date handling
                date_obj = datetime.datetime.now(tz=pytz.utc)
                date_obj = date_obj + datetime.timedelta(0, 120 * i)

                try:
                    # TODO: make dynamic based on file type
                    id3_obj = eyed3.load(filename)
                    file_title = id3_obj.tag.title
                    file_title = (filename if not file_title else file_title)
                except AttributeError:
                    file_title = filename

            fe.pubdate(date_obj)
            fe.published(date_obj)
            fe.id(str(podid))
            fe.title(file_title)
            fe.enclosure(type="audio/mpeg",
                         url=baseurl + filename,
                         length=str(os.path.getsize(filename)))
            i = i + 1

        rssfeed = fg.rss_str(pretty=True)
        fg.rss_file(rss_file_name)


app = RssDir()
app.cmdline_run()
